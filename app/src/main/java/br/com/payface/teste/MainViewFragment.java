package br.com.payface.teste;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class MainViewFragment extends Fragment {

    private String[] options = {"Alpha", "Sandbox", "Production", "Local"};

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_view, container, false);
        Spinner spinner = view.findViewById(R.id.main_spinner_environment);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, options);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (view != null && view.getContext() != null) {
                    if (view.getContext() instanceof MainActivity) {
                        MainActivity parent = (MainActivity) view.getContext();
                        parent.setEnvironment(position);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        EditText tvIdPartner = view.findViewById(R.id.main_et_id_partner);
        tvIdPartner.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d("dsfds", charSequence.toString());
                MainActivity parent = (MainActivity) getContext();
                assert parent != null;
                parent.setIdPartner(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        EditText tvCpf = view.findViewById(R.id.main_et_cpf);
        tvCpf.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                MainActivity parent = (MainActivity) getContext();
                assert parent != null;
                parent.setCpf(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        EditText tvName = view.findViewById(R.id.main_et_name);
        tvName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                MainActivity parent = (MainActivity) getContext();
                assert parent != null;
                parent.setName(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        EditText tvCellphone = view.findViewById(R.id.main_et_cellphone);
        tvCellphone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                MainActivity parent = (MainActivity) getContext();
                assert parent != null;
                parent.setCellphone(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        return view;
    }
}
