package br.com.payface.teste

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import br.com.payface.hybrid.HybridActivity

class HybridFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_one_button, container, false)

        val button = view.findViewById<Button>(R.id.a)
        button.setOnClickListener {
            val hybridActivity =  Intent(context, HybridActivity::class.java)
            var partner = (view.context as MainActivity).idPartner
            val cpf = (view.context as MainActivity).cpf
            val name = (view.context as MainActivity).name
            val cellphone = (view.context as MainActivity).cellphone
            val environment = (view.context as MainActivity).environment
            val button = "Voltar para aplicação"
            if (partner!= null) {
                hybridActivity.putExtra("partner", partner)
                hybridActivity.putExtra("cpf", cpf)
                hybridActivity.putExtra("name", name)
                hybridActivity.putExtra("cellphone", cellphone)
                hybridActivity.putExtra("environment", environment)
                hybridActivity.putExtra("button", button)

            }
            startActivity(hybridActivity)
        }

        return view
    }
}