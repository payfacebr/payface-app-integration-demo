package br.com.payface.teste

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment

class OneButtonFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_one_button, container, false)

        val button = view.findViewById<Button>(R.id.a)
        button.setOnClickListener {
            val openUrl = Intent(Intent.ACTION_VIEW)


            var partner = (view.context as MainActivity).idPartner
            val cpf = (view.context as MainActivity).cpf
            val name = (view.context as MainActivity).name
            val cellphone = (view.context as MainActivity).cellphone
            val environment = (view.context as MainActivity).environment
            var query = ""
            if (partner != "") {
                if (environment==3) {
                    query = "?partnerId=${partner}"
                }
                else {
                    partner +="."
                }
                if (cpf != "") {
                    query += if (environment==3) {
                        "&cpf=${cpf}"
                    } else {
                        "?cpf=${cpf}"
                    }
                    if (cellphone != "")
                        query += "&phone=${cellphone}"

                    if (name != "")
                        query += "&name=${name}"
                }
                else {
                    if (environment!=3) {
                        Toast.makeText(context, "CPF obrigatório para utilizar outros campos", Toast.LENGTH_LONG).show()
                    }
                }
            }
            else {
                Toast.makeText(context, "Pagina sem parceiro", Toast.LENGTH_LONG).show()
            }

            when (environment) {
                0 -> {
                    openUrl.data = Uri.parse("https://${partner}alpha.payface.app/${query}")
                }
                1 -> {
                    openUrl.data = Uri.parse("https://${partner}sandbox.payface.app/${query}")
                }
                2 -> {
                    openUrl.data = Uri.parse("https://${partner}payface.app/${query}")
                }
                3 -> {
                    openUrl.data = Uri.parse("http://127.0.0.1:3002/${query}")
                }
            }
            startActivity(openUrl)
        }

        return view
    }
}