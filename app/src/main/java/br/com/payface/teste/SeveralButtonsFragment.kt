package br.com.payface.teste

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast

class SeveralButtonsFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_several_buttons, container, false)
        val button1 = view.findViewById<Button>(R.id.button1)
        val button2 = view.findViewById<Button>(R.id.button2)
        val button3 = view.findViewById<Button>(R.id.button3)
        val button4 = view.findViewById<Button>(R.id.button4)

        button1.setOnClickListener {
            val openUrl = Intent(Intent.ACTION_VIEW)
            openUrl.data = Uri.parse(getBase(view)+"home"+getQuery(view))
            startActivity(openUrl)
        }

        button2.setOnClickListener {
            val openUrl = Intent(Intent.ACTION_VIEW)
            openUrl.data = Uri.parse(getBase(view)+"carteira"+getQuery(view))
            startActivity(openUrl)
        }

        button3.setOnClickListener {
            val openUrl = Intent(Intent.ACTION_VIEW)
            openUrl.data = Uri.parse(getBase(view)+"pagamentos"+getQuery(view))
            startActivity(openUrl)
        }

        button4.setOnClickListener {
            val openUrl = Intent(Intent.ACTION_VIEW)
            openUrl.data = Uri.parse(getBase(view)+"perfil"+getQuery(view))
            startActivity(openUrl)
        }
        return view
    }

    fun getBase(view: View) : String {
        var base = ""
        var partner = (view.context as MainActivity).idPartner
        val environment = (view.context as MainActivity).environment
        if (environment!=3 && partner != "") {
            partner += "."
        }
        when (environment) {
            0 -> {
                base = "https://${partner}alpha.payface.app/"
            }
            1 -> {
                base = "https://${partner}sandbox.payface.app/"
            }
            2 -> {
                base = "https://${partner}payface.app/"
            }
            3 -> {
                base = "http://127.0.0.1:3002/"
            }
        }
        return base
    }

    fun getQuery(view :View) : String {
        val cpf = (view.context as MainActivity).cpf
        val name = (view.context as MainActivity).name
        val cellphone = (view.context as MainActivity).cellphone
        val environment = (view.context as MainActivity).environment
        val partner = (view.context as MainActivity).idPartner
        var query = ""
        if (partner != "") {
            if (environment==3) {
                query = "?partnerId=${partner}"
            }
            if (cpf != "") {
                query += if (environment==3) {
                    "&cpf=${cpf}"
                } else {
                    "?cpf=${cpf}"
                }
                if (cellphone != "")
                    query += "&phone=${cellphone}"

                if (name != "")
                    query += "&name=${name}"
            }
            else {
                if (environment!=3) {
                    Toast.makeText(context, "CPF obrigatório para utilizar outros campos", Toast.LENGTH_LONG).show()
                }
            }
        }
        else {
            Toast.makeText(context, "Pagina sem parceiro", Toast.LENGTH_LONG).show()
        }

        return query
    }
}