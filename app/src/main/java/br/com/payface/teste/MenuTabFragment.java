package br.com.payface.teste;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

public class MenuTabFragment extends Fragment {
    MenuTabAdapter menuTabAdapter;
    ViewPager2 viewPager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_menu_tab, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        menuTabAdapter = new MenuTabAdapter(this);
        viewPager = view.findViewById(R.id.menu_tab_pager);
        viewPager.setAdapter(menuTabAdapter);
        TabLayout tabLayout = view.findViewById(R.id.menu_tab_tl_navigation_tab);
        new TabLayoutMediator(tabLayout, viewPager, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                switch (position) {
                    case 0:
                        tab.setText(getString(R.string.tab1));
                        break;
                    case 1:
                        tab.setText(getString(R.string.tab2));
                        break;
                    case 2:
                        tab.setText(getString(R.string.tab3));
                        break;
                }

            }
        }).attach();

    }
}
