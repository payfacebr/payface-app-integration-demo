package br.com.payface.hybrid

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import java.io.ByteArrayOutputStream
import java.io.File


class HybridActivity : AppCompatActivity() {

    private var partner : String? = null
    private var cpf : String? = null
    private var name : String? = null
    private var cellphone : String? = null
    private var button : String? = null
    private var environment = 0
    private var myWebView: WebView? = null
    private var btBack: Button? = null
    private val javascriptObj = "javascript_obj"
    private var cameraOpened = false


    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hybrid)
        closeCameraFragment()

        if (!allPermissionsGranted()) {
            requestPermission()
        }

        partner = intent.extras?.getString("partner")
        cpf = intent.extras?.getString("cpf")
        name = intent.extras?.getString("name")
        cellphone = intent.extras?.getString("cellphone")
        button = intent.extras?.getString("button", "off")
        environment = intent.extras?.getInt("environment", 2)!!

        btBack = findViewById(R.id.hybrid_payface_button)

        if (button.equals("off")) {
            btBack?.visibility = View.GONE
        }
        else {
            btBack?.text = button
            btBack?.setOnClickListener {
                super.onBackPressed()
            }
        }

        WebView.setWebContentsDebuggingEnabled(true)

        myWebView = findViewById(R.id.webview)
        myWebView?.settings?.javaScriptEnabled = true
        myWebView?.settings?.domStorageEnabled = true
        myWebView?.addJavascriptInterface(WebViewInterface(this), javascriptObj)
        myWebView?.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                Log.d("WebViewFragment", "Set injected function on webapp")
                injectJavaScriptFunction()
            }
        }
        var query = ""
        if (partner != "") {
            if (environment==3) {
                query = "?partnerId=${partner}"
            }
            else {
                partner +="."
            }
            if (cpf != "") {
                query += if (environment==3) {
                    "&cpf=${cpf}"
                } else {
                    "?cpf=${cpf}"
                }
                if (cellphone != "")
                    query += "&phone=${cellphone}"

                if (name != "")
                    query += "&name=${name}"
            }
            else {
                if (environment!=3) {
                    Log.d("HybridActivity", "CPF obrigatório para utilizar outros campos")
                }
            }
        }
        else {
            Log.d("HybridActivity", "Pagina sem parceiro")
        }

        var url = "https://alpha.payface.app/"
        when (environment) {
            0 -> {
                url = "https://${partner}alpha.payface.app/${query}"
            }
            1 -> {
                url = "https://${partner}sandbox.payface.app/${query}"
            }
            2 -> {
                url = "https://${partner}web.payface.app/${query}"
            }
            3 -> {
                url = "http://127.0.0.1:3002/${query}"
            }
        }

        myWebView?.loadUrl(url)
        Log.d("URL", url)

    }

    override fun onDestroy() {
        myWebView?.removeJavascriptInterface(javascriptObj)
        super.onDestroy()
    }

    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(
                baseContext, it) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
                this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (!allPermissionsGranted()) {
                Toast.makeText(this,
                        "Sem permissão para utilizar a câmera",
                        Toast.LENGTH_SHORT).show()
                requestPermission()
            }
        }
    }

    companion object {
        const val REQUEST_CODE_PERMISSIONS = 10
        private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)
    }

    private fun injectJavaScriptFunction() {
        myWebView?.evaluateJavascript("javascript: " +
                "window.payface.NATIVE_CAMERA_AVAILABLE = true;" +
                "window.payface.openNativeCameraForFaceCapture = function() { " +
                "${javascriptObj}.openNativeCameraForFaceCapture() };", null)
    }

    fun sendPicture(pic: String) {
        val file = File(pic)
        if (file.exists()) {
            val bmOptions = BitmapFactory.Options()
            val photo = BitmapFactory.decodeFile(pic, bmOptions)
            val matrix = Matrix()
            matrix.postRotate(-90f)
            val width: Int = photo.width
            val height: Int = photo.height
            val rotatedPhoto = Bitmap.createBitmap(photo, 0, 0, width, height, matrix, true)

            val stream = ByteArrayOutputStream()
            rotatedPhoto?.compress(Bitmap.CompressFormat.JPEG, 100, stream)
            val byteArray = stream.toByteArray()
            photo?.recycle()
            var base64Picture = Base64.encodeToString(byteArray, Base64.DEFAULT)
            base64Picture = base64Picture.replace("\n", "")
            val image64 = "data:image/jpeg;base64,$base64Picture"
            myWebView?.evaluateJavascript("javascript: nativeCameraFaceCaptured(\"$image64\")", null)
        }
    }


    fun openCameraFragment() {
        val fm = supportFragmentManager
        val cameraFragment = fm.findFragmentById(R.id.camera)
        (cameraFragment as CameraFragment).startCamera()
        fm.beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .show(cameraFragment)
                .commit()
        cameraOpened = true

    }

    fun closeCameraFragment() {
        val fm = supportFragmentManager
        val cameraFragment = fm.findFragmentById(R.id.camera)
        (cameraFragment as CameraFragment).closeCamera()
        fm.beginTransaction()
                .hide(cameraFragment)
                .commit()
        cameraOpened = false
    }

    override fun onBackPressed() {
        if (cameraOpened) {
            closeCameraFragment()
        }
        if (myWebView!!.canGoBack())
            myWebView?.goBack()
        else {
            super.onBackPressed()
        }
    }
}