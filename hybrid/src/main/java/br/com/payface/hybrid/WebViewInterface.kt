package br.com.payface.hybrid

import android.util.Log
import android.view.View
import android.webkit.JavascriptInterface
import androidx.navigation.findNavController

class WebViewInterface(val activity : HybridActivity) {

    @JavascriptInterface
    fun openNativeCameraForFaceCapture() {
        Log.d("OPEN CAMERA", "OPEN CAMERA")
        activity.openCameraFragment()
    }
}